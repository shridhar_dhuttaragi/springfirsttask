package com.example.forecastproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Main {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int main_id;

    long temp;
    long feels_like;
    long temp_min;
    long temp_max;
    long pressure;
    long humidity;
    long sea_level;
    long temp_kf;

    @Column(name = "date")
    String dt_txt;

    @Transient
//    @OneToOne
    Details_List details_list;

}
