package com.example.forecastproject.controller;

import com.example.forecastproject.model.Main;
import com.example.forecastproject.service.ForecastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
public class ForecastDetailsController {

    @Autowired
    ForecastService forecastService;

    @RequestMapping("/")
    @ResponseBody
    public String home()
    {
        return "Welcome to WeatherForecast";
    }
    @RequestMapping("/forecast/{cityname}")
    @ResponseBody
    public Object scanforecast(@PathVariable("cityname") String cityname) throws Exception {
        return forecastService.forecastOfCities(cityname);
    }
    @RequestMapping("/weather/{cityname}")
    @ResponseBody
    public Object scanweather(@PathVariable("cityname") String cityname) throws Exception {
        return forecastService.weatherOfCities(cityname);
    }


    @RequestMapping("/data/averageofpressure/{cityname}")
    @ResponseBody
    public Object avgOfPressure(@PathVariable("cityname") String cityname) throws Exception {
       return forecastService.avgOfPressure(cityname);

    }

    @RequestMapping("/data/averageofdaily/{cityname}")
    @ResponseBody
    public Object avgOfDaily(@PathVariable("cityname") String cityname) throws Exception {
        return forecastService.averageOfDaily(cityname);
    }
    @RequestMapping("/data/averageofnightly/{cityname}")
    @ResponseBody
    public Object avgOfNightly(@PathVariable("cityname") String cityname) throws Exception {
        return forecastService.averageOfNightly(cityname);
    }

    @RequestMapping("/login")
    public String login()
    {
        return "login";
    }


}
