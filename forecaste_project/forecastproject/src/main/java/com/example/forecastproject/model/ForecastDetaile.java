package com.example.forecastproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ForecastDetaile implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int forecastDetails_pk_id;

    int cod;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_forecast_id",referencedColumnName = "forecastDetails_pk_id")
    List<Details_List> list;

    City city;



}
